import sys

from utils import *

args = sys.argv

if len(args) == 0:
    print('You have not passed any commands in!')
else:
    for a in args:
        if a == '--help':
            print('Basic command line program')
            print('Options:')
            print(' --help -> show this basic help menu.')
            print(' xml -> convert json to xml file.')
            print(' csv -> convert json to csv file.')
            print(' xml_to_json -> convert xml to json file.')
            print(' csv_to_json -> convert csv to json file.')
        elif a == 'xml':
            convert_to_xml()
        elif a == 'csv':
            convert_to_csv()
        elif a == 'xml_to_json':
            convert_xml_to_json()
        elif a == 'csv_to_json':
            convert_csv_to_json()

