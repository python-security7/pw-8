import json
import csv
import xml.etree.ElementTree as E
from dicttoxml import dicttoxml


def convert_to_xml():
    f = open('data.json')
    json_obj = json.load(f)
    xml = dicttoxml(json_obj).decode("utf-8")
    with open("data.xml", "w") as f:
        f.write(xml)
        f.close()


def convert_to_csv():
    f = open('data.json')
    data = json.load(f)

    data_file = open('data.csv', 'w')
    csv_writer = csv.writer(data_file)
    count = 0

    for val in data:
        if count == 0:
            header = val.keys()
            csv_writer.writerow(header)
            count += 1
        csv_writer.writerow(val.values())

    data_file.close()


def convert_xml_to_json():
    tree = E.parse('data.xml')
    root = tree.getroot()
    d = {}
    for child in root:
        if child.tag not in d:
            d[child.tag] = []
        dic = {}
        for child2 in child:
            if child2.tag not in dic:
                dic[child2.tag] = child2.text
        d[child.tag].append(dic)
    json_object = json.dumps(d['item'], indent=4)
    f = open("data.json", "w")
    f.write(json_object)
    f.close()


def convert_csv_to_json():
    json_array = []

    with open('data.csv', encoding='utf-8') as csv_f:
        csv_reader = csv.DictReader(csv_f)

        for row in csv_reader:
            json_array.append(row)

    with open('data.json', 'w', encoding='utf-8') as json_f:
        json_string = json.dumps(json_array, indent=4)
        json_f.write(json_string)
